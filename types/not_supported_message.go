package types

type NotSupportedMessage struct {
	*BaseMessage
}

func (m *NotSupportedMessage) Ack() *string {
	return nil
}
