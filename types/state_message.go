package types

type StateMessage struct {
	*BaseMessage
}

func (m *StateMessage) Ack() *string {
	s := "STATE " + m.Request.Get("receive") + " OK"
	return &s
}

func (m *StateMessage) State() int {
	return m.Request.GetAsInt("state")
}

func (m *StateMessage) GsmRemainState() string {
	return m.Request.Get("gsm_remain_state")
}
