package types

type HangupMessage struct {
	*BaseMessage
}

func (m *HangupMessage) Ack() *string {
	s := "HANGUP " + m.Request.Get("receive") + " OK"
	return &s
}

func (m *HangupMessage) Hangup() int {
	return m.Request.GetAsInt("hangup")
}

func (m *HangupMessage) Num() string {
	return m.Request.Get("num")
}
