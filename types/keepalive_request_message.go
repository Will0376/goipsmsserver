package types

// https://goip-pro.ru/files/uploads/PDF/goip_sms_Interface_www.goip-pro.ru.pdf

type KeepAliveRequestMessage struct {
	*BaseMessage
	AuthSuccess bool
}

func (m *KeepAliveRequestMessage) Ack() *string {
	f := func() string {
		if m.AuthSuccess {
			return "0;"
		} else {
			return "1;"
		}
	}
	s := "req:" + m.Request.Get("req") + ";status:" + f()
	return &s
}

func (m *KeepAliveRequestMessage) Req() int {
	return m.Request.GetAsInt("req")
}

func (m *KeepAliveRequestMessage) Password() string {
	return m.Request.Get("pass")
}

func (m *KeepAliveRequestMessage) Num() string {
	return m.Request.Get("num")
}

func (m *KeepAliveRequestMessage) Signal() int {
	return m.Request.GetAsInt("signal")
}

func (m *KeepAliveRequestMessage) GsmStatus() string {
	return m.Request.Get("gsm_status")
}

func (m *KeepAliveRequestMessage) VoipStatus() string {
	return m.Request.Get("voip_status")
}

func (m *KeepAliveRequestMessage) VoipState() string {
	return m.Request.Get("voip_state")
}

func (m *KeepAliveRequestMessage) RemainTime() int {
	return m.Request.GetAsInt("remain_time")
}

func (m *KeepAliveRequestMessage) Imei() int {
	return m.Request.GetAsInt("imei")
}

func (m *KeepAliveRequestMessage) Pro() string {
	return m.Request.Get("pro")
}

func (m *KeepAliveRequestMessage) Idle() int {
	return m.Request.GetAsInt("idle")
}

func (m *KeepAliveRequestMessage) DisableStatus() int {
	return m.Request.GetAsInt("disable_status")
}

func (m *KeepAliveRequestMessage) SmsLogin() string {
	return m.Request.Get("sms_login")
}

func (m *KeepAliveRequestMessage) SmbLogin() string {
	return m.Request.Get("smb_login")
}

func (m *KeepAliveRequestMessage) CellInfo() string {
	return m.Request.Get("cellinfo")
}

func (m *KeepAliveRequestMessage) Cgatt() string {
	return m.Request.Get("cgatt")
}
