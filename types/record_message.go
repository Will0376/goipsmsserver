package types

type RecordMessage struct {
	*BaseMessage
}

func (m *RecordMessage) Ack() *string {
	s := "RECORD " + m.Request.Get("receive") + " OK"
	return &s
}

func (m *RecordMessage) Record() int {
	return m.Request.GetAsInt("record")
}

func (m *RecordMessage) Dir() int {
	return m.Request.GetAsInt("dir")
}

func (m *RecordMessage) Num() string {
	return m.Request.Get("num")
}
