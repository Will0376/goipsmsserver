package types

// Message интерфейс с методами, которые должны быть реализованы
type Message interface {
	Attributes() map[string]interface{}
	ID() string
	Password() string
	Ack() *string
	GetRequest() Request
}
