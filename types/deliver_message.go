package types

type DeliverMessage struct {
	*BaseMessage
}

func (m *DeliverMessage) Ack() *string {
	s := "DELIVER " + m.Request.Get("receive") + " OK"
	return &s
}

func (m *DeliverMessage) Deliver() int {
	return m.Request.GetAsInt("deliver")
}

func (m *DeliverMessage) SmsNo() int {
	return m.Request.GetAsInt("sms_no")
}

func (m *DeliverMessage) State() int {
	return m.Request.GetAsInt("state")
}

func (m *DeliverMessage) Num() string {
	return m.Request.Get("num")
}
