package types

import (
	"net"
	"strings"
)

// Request структура для хранения запроса
type Request struct {
	Buffer string
	Addr   *net.UDPAddr
	data   map[string]any
}

func (r *Request) Get(key string) string {
	if value, ok := r.data[key]; ok {
		return value.(string)
	}
	return ""
}

func (r *Request) GetAsInt(key string) int {
	if value, ok := r.data[key].(int); ok {
		return value
	}
	return 0
}

func (r *Request) All() map[string]any {
	return r.data
}

func CreateNewRequest(data []byte, address net.UDPAddr) *Request {
	stringData := string(data)
	parse(stringData)
	return &Request{stringData, &address, parse(stringData)}
}

func parse(buffer string) map[string]any {
	// Разделяем строку по символу ';'
	arr := strings.Split(buffer, ";")
	data := make(map[string]any)

	for _, value := range arr {
		// Разделяем каждую часть по символу ':'
		parts := strings.SplitN(value, ":", 2)

		// Если нет ключа и значения, пропускаем итерацию
		if len(parts) < 2 {
			continue
		}

		key := strings.TrimSpace(parts[0])
		val := strings.TrimSpace(parts[1])

		if len(key) == 0 {
			continue
		}

		// Преобразуем ключ в нижний регистр
		data[strings.ToLower(key)] = val
	}

	return data
}
