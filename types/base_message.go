package types

type BaseMessage struct {
	Request *Request
}

func (m *BaseMessage) Attributes() map[string]interface{} {
	return m.Request.All()
}

func (m *BaseMessage) ID() string {
	return m.Request.Get("id")
}

func (m *BaseMessage) Password() string {
	return m.Request.Get("password")
}

func (m *BaseMessage) GetRequest() Request {
	return *m.Request
}
