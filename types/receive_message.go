package types

type ReceiveMessage struct {
	*BaseMessage
}

func (m *ReceiveMessage) Ack() *string {
	s := "RECEIVE " + m.Request.Get("receive") + " OK"
	return &s
}

func (m *ReceiveMessage) Receive() int {
	return m.Request.GetAsInt("receive")
}

func (m *ReceiveMessage) SrcNum() string {
	return m.Request.Get("srcnum")
}

func (m *ReceiveMessage) Msg() string {
	return m.Request.Get("msg")
}
