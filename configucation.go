package main

import (
	"embed"
	"encoding/json"
	"log"
)

type Config struct {
	ServerLogin    string `json:"serverLogin"`
	ServerPassword string `json:"serverPassword"`
	ServerPort     string `json:"serverPort"`
}

var LoadedConfig Config

//go:embed config.json
var configFile embed.FS

func LoadConfig() {
	data, err := configFile.ReadFile("config.json")
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(data, &LoadedConfig)
	if err != nil {
		log.Fatal(err)
	}
}
