package contactparser

import (
	"regexp"
	"strings"
)

// Init загружает контакты.
func Init() {
	LoadContacts()
}

// PrintMarkV2Text возвращает строку с именем контакта и нормализованным номером телефона.
func (c *Contact) PrintMarkV2Text(phoneNumber string) string {
	normalizedNumber := RemoveExcessFromPhoneNumber(phoneNumber)
	return c.PhoneName + " ||" + normalizedNumber + "||"
}

// NormalizeNumber нормализует номер телефона в зависимости от страны.
func (p *Phone) NormalizeNumber(phoneNumber string) string {
	if value, exists := NormalizerMap[p.PhoneCountry]; exists {
		return value(phoneNumber)
	}
	return phoneNumber
}

// FindNumberContact ищет контакт по номеру телефона.
func FindNumberContact(phoneNumber string) *Contact {
	phoneNumber = RemoveExcessFromPhoneNumber(phoneNumber)
	for _, contact := range LoadedContacts {
		for _, phone := range contact.Phones {
			phoneNumberLocal := phone.NormalizeNumber(phoneNumber)
			if phone.Phone == phoneNumberLocal {
				return &contact
			}
		}
	}
	return nil
}

// FindNumberName возвращает имя контакта по номеру телефона.
func FindNumberName(phoneNumber string) string {
	contact := FindNumberContact(phoneNumber)
	if contact == nil {
		return phoneNumber
	}
	return contact.PhoneName
}

func RemoveExcessFromPhoneNumber(phone string) string {
	re := regexp.MustCompile(`[ \-()+]`)
	return re.ReplaceAllString(phone, "")
}

func EscapeMarkdown(text string) string {
	replacer := strings.NewReplacer(
		`_`, `\_`,
		`*`, `\*`,
		`[`, `\[`,
		`]`, `\]`,
		`(`, `\(`,
		`)`, `\)`,
		`#`, `\#`,
		`+`, `\+`,
		`-`, `\-`,
		`!`, `\!`,
		`<`, `\<`,
		`>`, `\>`,
		"`", "\\`",
		`|`, `\|`,
		`.`, `\.`,
	)
	return replacer.Replace(text)
}
