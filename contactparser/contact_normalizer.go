package contactparser

import (
	"regexp"
	"strings"
)

type Normalizer func(string) string

var NormalizerMap = map[string]Normalizer{
	"RU": func(phone string) string {
		re := regexp.MustCompile(`^\+7`)
		phone = re.ReplaceAllString(phone, "7")

		// Проверяем и заменяем префикс 8 на 7, если он находится в начале строки
		if strings.HasPrefix(phone, "8") {
			phone = "7" + phone[1:]
		}
		return phone
	},
}
