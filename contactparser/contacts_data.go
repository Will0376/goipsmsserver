package contactparser

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
)

type Phone struct {
	PhoneCountry string `json:"phoneCountry"`
	Phone        string `json:"phone"`
}

type Contact struct {
	PhoneName string  `json:"phoneName"`
	Phones    []Phone `json:"phones"`
}

var LoadedContacts []Contact

func LoadContacts() {
	executable, err := os.Executable()
	if err != nil {
		fmt.Println("Error getting path to executable file:", err)
		return
	}

	filePath := filepath.Join(filepath.Dir(executable), "contacts.json")

	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		fmt.Println("Created new contacts.json")
		createContactsFile(filePath)
		return
	}

	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&LoadedContacts)
	if err != nil {
		fmt.Println("Error parsing JSON:", err)
		return
	}
}

func createContactsFile(filePath string) {
	file, err := os.Create(filePath)
	if err != nil {
		fmt.Println("Error creating file:", err)
		return
	}
	defer file.Close()

	testContact := Contact{
		PhoneName: "Тестовый абонент",
		Phones: []Phone{
			{PhoneCountry: "RU", Phone: "71234567891"},
		},
	}
	LoadedContacts = []Contact{testContact}

	data, err := json.MarshalIndent(LoadedContacts, "", "    ")
	if err != nil {
		fmt.Println("Error when marshaling to JSON:", err)
		return
	}

	// Записываем в файл
	_, err = file.Write(data)
	if err != nil {
		fmt.Println("Error writing to file:", err)
		return
	}
}
