package ipromts

import (
	"fmt"
	"strconv"
	"strings"
)

func CO(name string, desc string, required bool, optionType OptionType) *Option {
	return &Option{Name: name, Description: desc, Required: required, Type: optionType}
}

func CORS(name string, desc string) *Option {
	return CO(name, desc, true, String)
}

func CORSS(name string, desc string) *Option {
	return CO(name, desc, true, StringSlice)
}

func CORB(name string, desc string) *Option {
	return CO(name, desc, true, Bool)
}

func CORI(name string, desc string) *Option {
	return CO(name, desc, true, Int)
}

func COS(name string, desc string) *Option {
	return CO(name, desc, false, String)
}

func COSS(name string, desc string) *Option {
	return CO(name, desc, false, StringSlice)
}

func COB(name string, desc string) *Option {
	return CO(name, desc, false, Bool)
}

func COI(name string, desc string) *Option {
	return CO(name, desc, false, Int)
}

func parseOptions(options []*Option, args []string) ([]ParsedOption, error) {
	var parsedOptions []ParsedOption
	requiredOptions := map[string]bool{}

	for _, opt := range options {
		if opt.Required {
			requiredOptions[opt.Name] = true
		}
	}

	i := 0
	for i < len(args) {
		found := false
		for _, opt := range options {
			if args[i] == opt.Name {
				if opt.Type == Bool {
					parsedOptions = append(parsedOptions, ParsedOption{Name: opt.Name, Value: true})
					delete(requiredOptions, opt.Name)
					i++
					found = true
					break
				} else if i+1 < len(args) {
					value, err := parseValue(opt.Type, args[i+1:])
					if err != nil {
						return nil, fmt.Errorf("invalid value for option %s: %v", opt.Name, err)
					}
					parsedOptions = append(parsedOptions, ParsedOption{Name: opt.Name, Value: value})
					delete(requiredOptions, opt.Name)
					// Skip consumed arguments
					i += consumedArgs(opt.Type, args[i+1:])
					found = true
					break
				} else {
					return nil, fmt.Errorf("option %s requires a value", opt.Name)
				}
			}
		}
		if !found {
			i++
		}
	}

	if len(requiredOptions) > 0 {
		var missingOptions []string
		for opt := range requiredOptions {
			missingOptions = append(missingOptions, opt)
		}
		return nil, fmt.Errorf("missing required options: %v", missingOptions)
	}

	return parsedOptions, nil
}

func parseValue(optionType OptionType, args []string) (interface{}, error) {
	switch optionType {
	case Int:
		return strconv.Atoi(args[0])
	case String:
		return args[0], nil
	case StringSlice:
		return parseStringSlice(args), nil
	case Bool:
		return true, nil
	default:
		return nil, fmt.Errorf("unsupported option type")
	}
}

func parseStringSlice(args []string) []string {
	var result []string
	var buffer string
	inQuotes := false

	for _, arg := range args {
		if strings.HasPrefix(arg, "\"") && !inQuotes {
			inQuotes = true
			buffer = arg[1:]
		} else if strings.HasSuffix(arg, "\"") && inQuotes {
			inQuotes = false
			buffer += " " + arg[:len(arg)-1]
			result = append(result, buffer)
			buffer = ""
		} else if inQuotes {
			buffer += " " + arg
		} else {
			result = append(result, arg)
		}
	}

	return result
}

func consumedArgs(optionType OptionType, args []string) int {
	switch optionType {
	case Int, String:
		return 2 // Option + value
	case StringSlice:
		for i, arg := range args {
			if strings.HasSuffix(arg, "\"") {
				return i + 2 // Option + values within quotes
			}
		}
		return len(args) + 1
	case Bool:
		return 1
	default:
		return 1
	}
}
