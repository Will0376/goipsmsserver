package ipromts

import (
	"fmt"
	"github.com/c-bata/go-prompt"
	"strings"
)

type OptionType int

const (
	Int OptionType = iota
	String
	StringSlice
	Bool
)

type Option struct {
	Name        string
	Description string
	Required    bool
	Type        OptionType
}

type Command struct {
	Name        string
	Description string
	Subcommands []*Command
	Execute     func(args []string, opts []ParsedOption)
	Options     []*Option
}

type ParsedOption struct {
	Name  string
	Value interface{}
}

func executeCommand(cmd *Command, args []string) {
	parsedOptions, err := parseOptions(cmd.Options, args)
	if err != nil {
		fmt.Println(err)
		return
	}

	if len(args) == 0 {
		if cmd.Execute != nil {
			cmd.Execute(args, parsedOptions)
		} else {
			fmt.Printf("Command: %s\nDescription: %s\n", cmd.Name, cmd.Description)
		}
		return
	}

	for _, subCmd := range cmd.Subcommands {
		if subCmd.Name == args[0] {
			executeCommand(subCmd, args[1:])
			return
		}
	}

	if cmd.Execute != nil {
		cmd.Execute(args, parsedOptions)
	} else {
		fmt.Printf("Command: %s\nDescription: %s\n", cmd.Name, cmd.Description)
	}
}

func findSuggestions(args []string, commands []*Command, suggestions *[]prompt.Suggest) {
	if len(args) == 0 {
		return
	}

	for _, cmd := range commands {
		if cmd.Name == args[0] {
			if len(args) == 1 {
				for _, subCmd := range cmd.Subcommands {
					*suggestions = append(*suggestions, prompt.Suggest{Text: subCmd.Name, Description: subCmd.Description})
				}
				for _, opt := range cmd.Options {
					*suggestions = append(*suggestions, prompt.Suggest{Text: opt.Name, Description: opt.Description})
				}
			} else {
				findSubSuggestions(args[1:], cmd, suggestions)
			}
			return
		}
	}
}

func getRootSuggests() []prompt.Suggest {
	var suggests []prompt.Suggest
	for _, forCommand := range rootCommands {
		suggests = append(suggests, prompt.Suggest{
			Text:        forCommand.Name,
			Description: forCommand.Description,
		})
	}
	return suggests
}

func findSubSuggestions(args []string, cmd *Command, suggestions *[]prompt.Suggest) {
	if len(args) == 0 {
		return
	}

	var currentArg string
	if len(args) > 0 {
		currentArg = args[0]
	}

	isOption := false
	for _, o := range cmd.Options {
		if o.Name == currentArg && o.Type == Bool {
			isOption = true
			break
		}
	}

	if isOption {
		if len(args) > 1 {
			findSubSuggestions(args[1:], cmd, suggestions)
		}
	} else {
		for _, subCmd := range cmd.Subcommands {
			if strings.HasPrefix(subCmd.Name, currentArg) {
				if hasSliceContainText(subCmd.Name, suggestions) == true {
					continue
				}
				*suggestions = append(*suggestions, prompt.Suggest{Text: subCmd.Name, Description: subCmd.Description})
			}
		}
		for _, o := range cmd.Options {
			if strings.HasPrefix(o.Name, currentArg) {
				if hasSliceContainText(o.Name, suggestions) == true {
					continue
				}
				*suggestions = append(*suggestions, prompt.Suggest{Text: o.Name, Description: o.Description})
			}
		}
		if len(args) > 1 {
			findSubSuggestions(args[1:], cmd, suggestions)
		}
	}
}

func hasSliceContainText(name string, suggestions *[]prompt.Suggest) bool {
	if *suggestions == nil {
		return false
	}

	for _, t := range *suggestions {
		if t.Text == name {
			return true
		}
	}
	return false
}
