package ipromts

import (
	"fmt"
	"github.com/c-bata/go-prompt"
	"strings"
)

func Init() {
	p := prompt.New(executor, completer)
	p.Run()
}

var rootCommands = []*Command{
	exitCmd,
}

func executor(input string) {
	args := strings.Fields(input)
	if len(args) == 0 {
		return
	}

	for _, cmd := range rootCommands {
		if cmd.Name == args[0] {
			executeCommand(cmd, args[1:])
			return
		}
	}

	fmt.Println("Command not found")
}

func completer(d prompt.Document) []prompt.Suggest {
	text := d.TextBeforeCursor()
	args := strings.Fields(text)

	if len(args) == 0 {
		return prompt.FilterHasPrefix(getRootSuggests(), text, true)
	}

	spaceCounter := strings.Count(text, " ")
	if spaceCounter == 0 {
		return prompt.FilterHasPrefix(getRootSuggests(), args[0], true)
	}

	var suggestions []prompt.Suggest
	findSuggestions(args, rootCommands, &suggestions)
	return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
}
