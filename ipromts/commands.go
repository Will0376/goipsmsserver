package ipromts

import (
	"log"
	"os"
)

var exitCmd = &Command{
	Name:        "exit",
	Description: "Exit from program",
	Subcommands: []*Command{},
	Execute:     exitExecute,
}

func exitExecute(args []string, opts []ParsedOption) {
	log.Println("Bye!")
	os.Exit(0)
}
