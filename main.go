package main

import (
	"GOIPSMSServer/contactparser"
	"GOIPSMSServer/ipromts"
	"GOIPSMSServer/types"
	"github.com/davecgh/go-spew/spew"
	"log"
	"net"
	"os"
)

func handleUDPConnection(conn *net.UDPConn) {
	defer func(conn *net.UDPConn) {
		err := conn.Close()
		if err != nil {
			panic(err)
		}
	}(conn)
	buf := make([]byte, 4096)
	for {
		n, addr, err := conn.ReadFromUDP(buf)
		if err != nil {
			log.Println("Error reading:", err)
			return
		}
		bytes := buf[:n]
		request := types.CreateNewRequest(bytes, *addr)
		HandleMessage(*request, conn)
	}
}

func main() {
	log.SetOutput(os.Stdout)
	log.SetFlags(log.LstdFlags)

	LoadConfig()
	go contactparser.Init()

	udpAddr, err := net.ResolveUDPAddr("udp", ":"+LoadedConfig.ServerPort)
	if err != nil {
		log.Println("Error resolving UDP address:", err)
		return
	}
	udpListener, err := net.ListenUDP("udp", udpAddr)
	if err != nil {
		log.Println("Error listening for UDP connections:", err)
		return
	}
	defer func(udpListener *net.UDPConn) {
		err := udpListener.Close()
		if err != nil {
			panic(err)
		}
	}(udpListener)

	log.Println("Listening for connections on port " + LoadedConfig.ServerPort)

	go func() {
		for {
			handleUDPConnection(udpListener)
		}
	}()

	ListenAllMessages(func(message types.Message) {
		spew.Dump(message)
	})

	ListenMessage(types.ReceiveMessage{}, func(message types.Message) {
		if rm, ok := message.(*types.ReceiveMessage); ok {
			log.Println("Incoming message from ", rm.SrcNum(), " with message: "+rm.Msg())
		} else {
			panic("Wrong type for message types.KeepAliveRequestMessage[0]")
		}
	})

	ipromts.Init()
}
