package main

import (
	"GOIPSMSServer/types"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"net"
	"sync"
)

var (
	bannedIpsMu sync.Mutex
	bannedIps   = make(map[string]struct{})
)

func HandleMessage(request types.Request, conn *net.UDPConn) {
	message := createType(request)
	if message == nil {
		return
	}

	creditsValid := checkCredits(message)
	if kaReq, ok := message.(*types.KeepAliveRequestMessage); ok && !creditsValid {
		kaReq.AuthSuccess = false
	}

	sendACKBack(message, conn)
	if creditsValid {
		Dispatch(message)
	}
}

func sendACKBack(message types.Message, conn *net.UDPConn) {
	ack := message.Ack()
	if ack != nil {
		addr := message.GetRequest().Addr
		_, err := conn.WriteToUDP([]byte(*ack), addr)
		if err != nil {
			fmt.Println("Error sending ACK:", err)
			return
		}
	}
}

func checkCredits(message types.Message) bool {
	if message == nil {
		return false
	}

	addr := message.GetRequest().Addr
	stringIp := addr.IP.String()

	bannedIpsMu.Lock()
	defer bannedIpsMu.Unlock()

	if _, exists := bannedIps[stringIp]; exists {
		fmt.Println("Message from banned ip ", stringIp, " rejected")
		return false
	}

	login := message.ID()
	password := message.Password()

	if LoadedConfig.ServerLogin != login || LoadedConfig.ServerPassword != password {
		fmt.Println("Login or Password mismatch from ", stringIp, ":", addr.Port)
		bannedIps[stringIp] = struct{}{}
		return false
	}
	return true
}

func createType(request types.Request) types.Message {
	all := request.All()
	if len(all) == 0 {
		return nil
	}

	typeMap := map[string]func(*types.Request) types.Message{
		"req": func(req *types.Request) types.Message {
			return &types.KeepAliveRequestMessage{BaseMessage: &types.BaseMessage{Request: req}, AuthSuccess: true}
		},
		"state": func(req *types.Request) types.Message {
			return &types.StateMessage{BaseMessage: &types.BaseMessage{Request: req}}
		},
		"record": func(req *types.Request) types.Message {
			return &types.RecordMessage{BaseMessage: &types.BaseMessage{Request: req}}
		},
		"hangup": func(req *types.Request) types.Message {
			return &types.HangupMessage{BaseMessage: &types.BaseMessage{Request: req}}
		},
		"receive": func(req *types.Request) types.Message {
			return &types.ReceiveMessage{BaseMessage: &types.BaseMessage{Request: req}}
		},
		"deliver": func(req *types.Request) types.Message {
			return &types.DeliverMessage{BaseMessage: &types.BaseMessage{Request: req}}
		},
	}

	for key, constructor := range typeMap {
		if _, ok := all[key]; ok {
			return constructor(&request)
		}
	}

	fmt.Println("Unsupported message type:")
	spew.Dump(request)
	return &types.NotSupportedMessage{BaseMessage: &types.BaseMessage{Request: &request}}
}
