package main

import (
	"GOIPSMSServer/types"
	"reflect"
	"sync"
)

var (
	listeners     = make(map[string][]Listener)
	listenersLock sync.RWMutex
)

const AllMessagesType = "ALL"

type Listener func(message types.Message)

func ListenAllMessages(listener Listener) {
	addNewListener(AllMessagesType, listener)
}

func ListenMessage(typeInterface interface{}, listener Listener) {
	typeName := reflect.TypeOf(typeInterface).String()
	addNewListener(typeName, listener)
}

func Dispatch(message types.Message) {
	typeName := reflect.TypeOf(message).Elem().String()
	var foundListeners []Listener

	listenersLock.RLock()
	foundListeners = append(foundListeners, listeners[typeName]...)
	foundListeners = append(foundListeners, listeners[AllMessagesType]...)
	listenersLock.RUnlock()
	for _, listener := range foundListeners {
		listener(message)
	}
}

func addNewListener(typeName string, listener Listener) {
	listenersLock.Lock()
	defer listenersLock.Unlock()
	listeners[typeName] = append(listeners[typeName], listener)
}
